import React from 'react'
import { useSelector, useDispatch } from 'react-redux';
import { modalState$ } from '../../redux/selectors';
import { Modal, TextField, TextareaAutosize, Button } from '@material-ui/core';
import useStyles from './styles';
import FileBase64 from 'react-file-base64';
import { hideModal,createPosts } from '../../redux/actions';
export default function CreatePostModal() {
    const [data, setData]=React.useState({
        content:'',
        title:'',
        attachment:''
    });
    const classes = useStyles();
    const dispatch=useDispatch();
    const onClose=React.useCallback(()=>{
        dispatch(hideModal())
    },[dispatch])
    const onSubmit=React.useCallback(()=>{
        console.log(data);
        dispatch(createPosts.createPostRequest(data));
    },[data, dispatch])
    const body = (
        <div className={classes.paper} id="simple-modal-title">
            <h2>Create new post</h2>
            <form noValidate autoComplete='off' className={classes.form}>
                <TextField
                    className={classes.title}
                    required
                    label='Title'
                    value={data.title}
                    onChange={(e)=>setData({...data, title:e.target.value})}
                />
                <TextareaAutosize
                    className={classes.textArea}
                    maxRows={15}
                    minRows={10}
                    placeholder='Content...'
                    onChange={(e)=>setData({...data, content:e.target.value})}
                    value={data.content} />
                <FileBase64
                    accept="image/*"
                    multiple={false}
                    type='file'
                    value={data.attachment}
                    onDone={({base64})=>setData({...data, attachment:base64})}
                />
                <div className={classes.footer}><Button variant='contained' color='primary' component='span' fullWidth onClick={onSubmit}>Create</Button></div>
            </form>
        </div>
    );
    const { isShow } = useSelector(modalState$);
    return (
        <div>
            <Modal open={isShow} onClose={onClose}>
                {body}
            </Modal>
        </div>
    )
}
