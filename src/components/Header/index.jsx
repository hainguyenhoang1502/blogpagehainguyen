import React from 'react'
import {Typography} from '@material-ui/core';
import useStyles from './styles';
export default function Header() {
    const classes=useStyles();
  return (
    <div>
      <Typography variant='h4' align='center' className={classes.container}>
      Blogs
    </Typography>
    </div>
  )
}
