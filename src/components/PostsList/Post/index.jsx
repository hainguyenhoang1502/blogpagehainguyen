import React from 'react'
import { Avatar, Card, CardActions, CardContent, CardHeader, CardMedia, IconButton, Typography } from '@material-ui/core'
import { Favorite } from "@material-ui/icons"
import{MoreVert} from "@material-ui/icons"
import moment from 'moment';
import useStyles from './styles';
import { useDispatch } from 'react-redux';
import { updatePost } from '../../../redux/actions';
export default function Post({post}) {
    const classes=useStyles();
    const dispatch=useDispatch();
    const handleLikeClick=React.useCallback(()=>{
        dispatch(updatePost.updatePostRequest({...post, linkCount:post.linkCount+1}))
    },[dispatch, post])
    return (
        <Card>
            <CardHeader avatar={<Avatar>A</Avatar>}
                title={post.author}
                subheader={moment(post.updatedAt).format("HH:MM DD MM YYYY") }
                action={
                    <IconButton>
                        <MoreVert />
                    </IconButton>
                } />
            <CardMedia image={post.attachment} className={classes.media} title="Title"/>
            <CardContent>
                <Typography variant='h5' color="textPrimary">{post.title}</Typography>
                <Typography variant='body2' component="p" color="textSecondary">{post.content}</Typography>
            </CardContent>
            <CardActions>
                <IconButton>
                    <Favorite onClick={handleLikeClick}/>
                    <Typography component='span' color="textSecondary">{post.linkCount}</Typography>
                </IconButton>
            </CardActions>
        </Card>
    )
}
