import React from 'react'
import { Container, Fab } from '@material-ui/core'
import Header from '../components/Header'
import PostsList from '../components/PostsList'
import Add from "@material-ui/icons/Add"
import useStyles from './styles'
import { useDispatch } from 'react-redux'
import { showModal } from '../redux/actions'
import CreatePostModal from '../components/CreatePostModal'
export default function Homepage() {
    const classes=useStyles();
    const dispatch=useDispatch();
    const openCreateModal=React.useCallback(()=>{
        dispatch(showModal())
    },[dispatch])
  return (
    <Container maxWidth='lg' className=''>
        <Header/>
        <PostsList/>
        <CreatePostModal/> 
        <Fab
            color='primary'
            className={classes.fab}
            onClick={openCreateModal}
        >
            <Add/>
        </Fab>
    </Container>
  )
}
