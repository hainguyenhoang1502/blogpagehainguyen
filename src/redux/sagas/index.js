import { takeLatest, call, put } from "redux-saga/effects";
import * as actions from '../actions';
import * as api from '../../api';

function* fetchPostSaga(action) {
    try {
        const posts = yield call(api.fetchPosts);
        console.log('post', posts);
        yield put(actions.getPosts.getPostSuccess(posts.data))
    } catch (err) {
        yield put(actions.getPosts.getPostFailure(err))
    }
}

function* createPostSaga(action) {
    try {
        const post = yield call(api.createPosts, action.payload);
        console.log('[createpostsaga', post);
        yield put(actions.createPosts.createPostSuccess(post.data))
        yield put(actions.hideModal());
    } catch (err) {
        yield put(actions.createPosts.createPostFailure(err))
    }
}

function* updatePostSaga(action) {
    try {
        const updatePost = yield call(api.updatePost, action.payload);
        console.log('[createpostsaga', updatePost);
        yield put(actions.updatePost.updatePostSuccess(updatePost.data))
    } catch (err) {
        yield put(actions.updatePost.updatePostFailure(err))
    }
}

function* mySaga() {
    yield takeLatest(actions.getPosts.getPostsRequest, fetchPostSaga)
    yield takeLatest(actions.createPosts.createPostRequest, createPostSaga)
    yield takeLatest(actions.updatePost.updatePostRequest, updatePostSaga)
}
export default mySaga;