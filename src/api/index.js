import axios from 'axios';
const url = 'https://sheltered-shore-56224.herokuapp.com';

export const fetchPosts = () => axios.get(`${url}/post`)
export const createPosts = (payload) => axios.post(`${url}/post`, payload);
export const updatePost = (payload) => axios.post(`${url}/post/update`, payload);