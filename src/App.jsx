import logo from './logo.svg';
import './App.css';
import { useDispatch } from 'react-redux';
import * as actions from './redux/actions'
import Homepage from './pages/Homepage';
function App() {

    return (
        <div><Homepage /></div>
    );
}

export default App;